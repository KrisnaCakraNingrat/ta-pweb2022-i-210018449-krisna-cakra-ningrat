<?php
	//listing program 9.1
	
	$gaji = 1000000;
	$pajak = 0.1;
	$bonus = 250000;
	$thp = $gaji - ($gaji * $pajak);
	$hasil = $thp + $bonus;

	echo "Gaji sebelum pajak = Rp. $gaji <br>";
	echo "Gaji yang dibawa pulang = Rp. $thp <br>";
	echo "Gaji yang dibawa pulang setelah mendapatkan bonus = Rp. $hasil";
	echo "<hr><hr>";

	//listing program 9.2

	$a = 5;
	$b = 4;

	echo "$a == $b : " . ($a == $b);
	echo "<br>$a != $b : " . ($a != $b);
	echo "<br>$a > $b : " . ($a > $b);
	echo "<br>$a < $b : " . ($a < $b);
	echo "<br>($a == $b) && ($a > $b) : " . (($a != $b) && ($a > $b));
	echo "<br>($a == $b) || ($a > $b) : " . (($a != $b) || ($a > $b));
	echo "<br>$a x $b == $b x $a : " . (($a * $b) == ($b * $a));
	echo "<br>$a x 2 > $b x 3 : " . (($a * 2) > ($b * 3));
	echo "<br>$a x 2 < $b x 3 : " . (($a * 2) < ($b * 3));

	echo "<br><br>Nilai 1 = true <br>Nilai 0 = false"
?>