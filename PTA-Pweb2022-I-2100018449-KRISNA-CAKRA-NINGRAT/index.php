<!DOCTYPE html>
<html>
<head>
	<title>TA PWEB KRISNA</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
	<div class="bg-loader">
		<div class="loader"></div>
	</div>

	<div class="medsos">
		<div class="container">
			<ul>
				<li><a href="https://api.whatsapp.com/send?phone=6282119508375&text=Hello Guys apa kabarnya???"><i>WhatsApp</i></a></li>
				<li><a href="https://github.com/KrisnaCakra"><i>GitHub</i></a></li>
				<li><a href="https://www.instagram.com/krisnacakna/"><i>Instagram</i></a></li>
			</ul>
		</div>
	</div>
	<header>
		<div class="container">
			<h1><a href="index.php">KRISNA CAKRA NINGRAT</a></h1>
			<ul>
				<li class="active"><a href="index.php">HOME</a></li>
				<li><a href="about.php">ABOUT</a></li>
				<li><a href="service.php">PRESENSI</a></li>
			</ul>
		</div>
	</header>

	<section class="banner">
		<h2>SELAMAT DATANG DI WEBSITE SAYA</h2>
	</section>

	<section class="about">
		<div class="container">
			<h3>ABOUT ME</h3>
			<p>Hai, selamat datang di website sederhana saya.<strong> Perkenalkan nama saya Krisna Cakra Ningrat, nama panggilan saya Krisna.</strong> Saya lahir di Kalimantan Selatan tepatnya di Banjarmasin, lahir pada tangga 10 April 2003. Sekarang saya menempuh dunia pendidikan di Universitas Ahmad Dahlan yang ada di kota pelajar Yogyakarta.</p>
		</div>
	</section>

	<footer>
		<div class="container">
			<small>Copyright &copy; 2022 - Krisna. All Rights Reserved.</small>
		</div>
	</footer>


	<script type="text/javascript">
		$(document).ready(function(){
			$(".bg-loader").hide();
		})
	</script>
</body>
</html>