<!DOCTYPE html>
<html>
<head>
	<title>TA PWEB KRISNA</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
	<div class="bg-loader">
		<div class="loader"></div>
	</div>

	<div class="medsos">
		<div class="container">
			<ul>
				<li><a href="https://api.whatsapp.com/send?phone=6282119508375&text=Hello Guys apa kabarnya???"><i>WhatsApp</i></a></li>
				<li><a href="https://github.com/KrisnaCakra"><i>GitHub</i></a></li>
				<li><a href="https://www.instagram.com/krisnacakna/"><i>Instagram</i></a></li>
			</ul>
		</div>
	</div>
	<header>
		<div class="container">
			<h1><a href="index.php">KRISNA CAKRA NINGRAT</a></h1>
			<ul>
				<li><a href="index.php">HOME</a></li>
				<li><a href="about.php">ABOUT</a></li>
				<li class="active"><a href="service.php">PRESENSI</a></li>
			</ul>
		</div>
	</header>

	<section class="label">
		<div class="container">
			<p>Home / Presensi</p>
		</div>
	</section>

	<div class="pesan">
			<h1>Presensi</h1>
			<form name="form1" method="post" action="proses.php" onSubmit="validasi()">
				<table width="30%" border="0" align="center">
					<tr>
						<td>Nama</td>
						<td><input name="nama" type="text" id="nama"></td>
					</tr>

					<tr>
						<td>Kelas</td>
						<td><input name="kelas" type="text" id="kelas"></td>
					</tr>

					<tr>
						<td>NIM</td>
						<td><input name="nim" type="text" id="nim"></td>
					</tr>

					<tr>
						<td>Institusi</td>
						<td><input name="institusi" type="text" id="institusi"></td>
					</tr>

					<tr>
						<td>Email</td>
						<td><input name="email" type="text" id="email"></td>
					</tr>

					<tr>
						<td></td>
						<td><input type="submit" name="Submit" value="Kirim"><input type="reset" name="Submit2" value="Batal"></td>
					</tr>
				</table>
			</form>
		</div>
		<footer>
		<div class="container">
			<small>Copyright &copy; 2022 - Krisna. All Rights Reserved.</small>
		</div>
	</footer>


	<script type="text/javascript">
		$(document).ready(function(){
			$(".bg-loader").hide();
		})
	</script>
</body>
</html>