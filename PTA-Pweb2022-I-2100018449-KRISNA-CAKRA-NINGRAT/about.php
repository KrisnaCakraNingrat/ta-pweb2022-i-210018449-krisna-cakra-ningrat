<!DOCTYPE html>
<html>
<head>
	<title>TA PWEB KRISNA</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
	<div class="bg-loader">
		<div class="loader"></div>
	</div>

	<div class="medsos">
		<div class="container">
			<ul>
				<li><a href="https://api.whatsapp.com/send?phone=6282119508375&text=Hello Guys apa kabarnya???"><i>WhatsApp</i></a></li>
				<li><a href="https://github.com/KrisnaCakra"><i>GitHub</i></a></li>
				<li><a href="https://www.instagram.com/krisnacakna/"><i>Instagram</i></a></li>
			</ul>
		</div>
	</div>
	<header>
		<div class="container">
			<h1><a href="index.php">KRISNA CAKRA NINGRAT</a></h1>
			<ul>
				<li><a href="index.php">HOME</a></li>
				<li class="active"><a href="about.php">ABOUT</a></li>
				<li><a href="service.php">PRESENSI</a></li>
			</ul>
		</div>
	</header>

	<section class="label">
		<div class="container">
			<p>Home / About</p>
		</div>
	</section>

	<section class="about">
		<div class="container">
			<img src="banner.jpeg" width="100%">
			<h3>ABOUT ME</h3>
			<p>Hai, selamat datang di website sederhana saya.<strong> Perkenalkan nama saya Krisna Cakra Ningrat, nama panggilan saya Krisna.</strong> Saya lahir di Kalimantan Selatan tepatnya di Banjarmasin, lahir pada tangga 10 April 2003. Sekarang saya menempuh dunia pendidikan di Universitas Ahmad Dahlan yang ada di kota pelajar Yogyakarta.</p>

			<p>Pada kesempatan kali ini saya ditugaskan untuk membuat web yang didalamnya terdapat HTML, PHP, CSS, dan Java Script. <strong>Penugasan ini bertujuan untuk membuat tugas akhir dari mata kuliah PWeb</strong>, walaupun banyak kekurangan dalam membuat web ini mohon dimaklumi karena masih dalam tahap perkembangan dan dalam pelatihan, agar bisa menjadi lebih profesional.</p>
			
			<h3>MOTTO</h3>
			<p>Pada web ini saya akan memberikan beberapa motto hidup sebagai penyemangat dihari-hari kita yang akan datang. Seperti berikut contohnya. <strong>Di setiap kesulitan pasti ada kemudahan</strong>. Lalu ada <strong>Tidak perlu khawatir dengan masa depan, sebab setiap orang memiliki waktunya masing-masing</strong></p>
		</div>
	</section>

	<footer>
		<div class="container">
			<small>Copyright &copy; 2022 - Krisna. All Rights Reserved.</small>
		</div>
	</footer>

	<script type="text/javascript">
		$(document).ready(function(){
			$(".bg-loader").hide();
		})
	</script>
</body>
</html>